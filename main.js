const addButtonEl = document.querySelector('.js-add__item');
const inputEl = document.querySelector('.js-input');
const listEl = document.querySelector('.js-list');

addButtonEl.addEventListener('click',
    e => {
        const text = inputEl.value;
        inputEl.value = '';
        const mode = e.target.getAttribute('mode');

        if (mode === 'add') {

            const li = document.createElement('li');

            li.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-start');

            li.innerHTML = `<div class="ms-2 me-auto js-item__text">
                        <input class = "js-items__checkbox line-through" type="checkbox"> 
                        <span>${text}</span> 
                        </div>
                    <span class="badge btn-warning rounded-pill js-edit"><i class="fas fa-pencil-alt"></i></span>
                    <span class="badge bg-danger rounded-pill js-close"><i class="fas fa-times"></i></span>`;

            listEl.append(li);
        }
        if (mode === 'edit') {
            document.querySelector('.active-element .js-item__text span').textContent = text;
            addButtonEl.textContent = 'Добавить';
            addButtonEl.classList.add('btn-primary');
            addButtonEl.classList.remove('btn-warning');
            addButtonEl.setAttribute('mode', 'add');
        }
    }
)


listEl.addEventListener(
    'click',
    e => {
        if (e.target.classList.contains('js-items__checkbox')) {
            e.target.parentNode.classList.toggle('line-through');
        }

        if (e.target.classList.contains('js-close')) {
            e.target.parentNode.remove(); // корректна ли такая запись?
            // listEl.removeChild(e.target.parentNode);
        }

        if (e.target.classList.contains('js-edit')) {
            const text = e.target.parentNode.querySelector('.js-item__text span').textContent;
            e.target.parentNode.classList.add('active-element');
            inputEl.value = text;
            addButtonEl.textContent = 'Обновить';
            addButtonEl.classList.remove('btn-primary');
            addButtonEl.classList.add('btn-warning');
            addButtonEl.setAttribute('mode', 'edit');

            addButtonEl.addEventListener(
                'click',
                function () {
                    addButtonEl.textContent = 'Добавить';
                    e.target.parentNode.classList.remove('active-element');
                }
            )
        }
    }
);









